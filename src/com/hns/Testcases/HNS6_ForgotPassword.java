package com.hns.Testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Utilities.ReadProperties;

import junit.framework.Assert;

public class HNS6_ForgotPassword {
	ReadProperties prop = new ReadProperties();
	Browser browser = new Browser();
	WebDriver driver = null;
	WebElement element = null;

	@BeforeClass
	public void before() throws Exception {
		driver = browser.LaunchBrowser(prop.getProperty("GlobalConfig.url"));
	}

	@Test
	public void forgotPassword() throws Exception {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		element = driver.findElement(By.xpath(prop.getProperty("HomePage.homeLogin")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("LoginPage.forgotPasswordLink")));
		element.click();

		element = driver.findElement(By.xpath(prop.getProperty("LoginPage.forgotPasswordEmailID")));
		element.clear();
		element.sendKeys("softtest42@gmail.com");
		element = driver.findElement(By.xpath(prop.getProperty("LoginPage.forgotPasswordSubmit")));
		element.click();

		Thread.sleep(5000);
		element = driver.findElement(By.xpath(prop.getProperty("LoginPage.statusMessage")));
		String statusMessage = element.getText();
		Assert.assertEquals("", statusMessage);
	}
}
