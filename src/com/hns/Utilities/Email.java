package com.hns.Utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SubjectTerm;

public class Email {
	
	private Message getEmail(String emailID, String password, String subjectToBeSearched) throws Exception {
	Properties props = System.getProperties();
    props.setProperty("mail.store.protocol", "imaps");

        Session session = Session.getDefaultInstance(props, null);
        Store store = session.getStore("imaps");
        store.connect("imap.gmail.com", emailID, password);

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_WRITE);

       // System.out.println("Total Message:" + folder.getMessageCount());
       // System.out.println("Unread Message:"
       //         + folder.getUnreadMessageCount());
        
        Message[] messages = null;
        boolean mailFound = false;
        Message email= null;
    	
    	
        for (int i = 0; i < 30; i++) {
            messages = folder.search(new SubjectTerm(subjectToBeSearched),folder.getMessages());
            if (messages.length == 0) {
                Thread.sleep(10000);
            }
        }

        for (Message mail : messages) {
            if (!mail.isSet(Flags.Flag.SEEN)) {
                email = mail;
               // System.out.println("Message Count is: "
               //         + email.getMessageNumber());
                mailFound = true;
            }
        }

        if (!mailFound) {
            throw new Exception(
                    "Could not found Email");
        
        } 
                                     
        
		return email;
	}       
	
	
	public String getSignUpLink(String emailID, String password) throws Exception {
		String signUpLink = null;
		String temp = null;
        String line;
        Message email = getEmail(emailID, password, "Welcome to Hone and Strop");
		BufferedReader reader = new BufferedReader(
                 new InputStreamReader(email
                         .getInputStream()));
       
		while ((line = reader.readLine()) != null) {
         	//System.out.println(line);
         	if(line.contains("Log in to my account")) {
         		temp = line;
         		break;
         	}
		}
	
        signUpLink = temp.substring(temp.indexOf("http://"), temp.indexOf("style"));
    	return signUpLink;
	}
	
	public String getPasswordResetLink(String emailID, String password) throws Exception {
		String passwordResetLink = null;
        String line;
        Message email = getEmail(emailID, password, "Replacement login information for");
		BufferedReader reader = new BufferedReader(
                 new InputStreamReader(email
                         .getInputStream()));
       
		while ((line = reader.readLine()) != null) {
         	System.out.println(line);
         	if(line.contains("/user/reset")) {
         		passwordResetLink = line;
         		break;
         	}
		}
         	
        passwordResetLink = passwordResetLink.substring(passwordResetLink.indexOf("http://"),passwordResetLink.indexOf(" "));
        System.out.println( "SignUp Link = " + passwordResetLink);   	
         	
		return passwordResetLink;
	}
	
}
