package com.hns.Utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {
	
	Properties prop = new Properties();
	InputStream input = null;
	
	public String getProperty(String elements) throws Exception
	{
		String[] properties = elements.split("\\.");
		input = new FileInputStream("src/com/hns/ObjectRepo/"+properties[0]+".properties");
		prop.load(input);
		
		if(prop.getProperty(properties[1]) == null)
		{
			throw new Exception("Key '" + properties[1] + "' not found in Object Repository");
		}
		
		return prop.getProperty(properties[1]);
	}

	
}
